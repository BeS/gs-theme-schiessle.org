Minimalistic GNU Social Theme for schiessle.org
===============================================

A [GNU social](http://gnu.io/social/) Theme based on the [Pretty-Simple theme](https://github.com/chimo/gs-prettySimple).

It contains quite some custom stuff, especially for the footer and the global menu [here](http://io.schiessle.org).
